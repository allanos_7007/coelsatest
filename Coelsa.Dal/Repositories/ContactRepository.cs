﻿using Coelsa.Dal.Core;
using Coelsa.Dal.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Coelsa.Dal.Repositories
{
    public class ContactRepository : BaseRepository<Contact>, IContactRepository
    {
        public ContactRepository(CoelsaDbContext dbContext)
        : base(dbContext)
        { }
    }
}
