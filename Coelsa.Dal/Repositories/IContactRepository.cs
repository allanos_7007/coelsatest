﻿using Coelsa.Dal.Core;
using Coelsa.Dal.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Coelsa.Dal.Repositories
{
    public interface IContactRepository : IBaseRepository<Contact>
    {
    }
}
