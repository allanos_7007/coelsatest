﻿using Coelsa.Dal.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Coelsa.Dal.Core
{
    public class CoelsaDbContext : DbContext 
    {
        public CoelsaDbContext(DbContextOptions<CoelsaDbContext> options)
        : base(options)
        { }

        public DbSet<Contact> Contact { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Contact>().HasIndex(u => u.Id).IsUnique();
            base.OnModelCreating(modelBuilder);
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            SetAudit();
            return base.SaveChangesAsync(cancellationToken);
        }

        private void SetAudit()
        {
            var hour = DateTime.UtcNow.AddHours(-3);
            foreach (var item in ChangeTracker.Entries().Where(e => e.State == EntityState.Added && e.Entity is Audit))
            {
                var audit = item.Entity as Audit;
                audit.Created = hour;
                audit.Modify = hour;
            }
        }
    }
}
