﻿using Coelsa.Dal.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Coelsa.Dal.Core
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly CoelsaDbContext _context;

        private ContactRepository _contactRepository;

        public UnitOfWork(CoelsaDbContext context)
        {
            this._context = context;
        }

        public IContactRepository Contact => this._contactRepository = this._contactRepository ?? new ContactRepository(this._context);

        public async Task<bool> CommitAsync()
        {
            await this._context.SaveChangesAsync();

            return true;
        }

        public void Dispose()
        {
            this._context.Dispose();
        }
    }
}
