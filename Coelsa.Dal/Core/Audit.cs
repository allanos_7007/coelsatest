﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Coelsa.Dal.Core
{
    public abstract class Audit
    {
        public DateTime Created { get; set; }
        public DateTime Modify { get; set; }
    }
}
