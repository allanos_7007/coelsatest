﻿using Coelsa.Dal.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Coelsa.Dal.Core
{
    public interface IUnitOfWork : IDisposable
    {
        IContactRepository Contact { get; }
        Task<bool> CommitAsync();
    }
}
