﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Coelsa.Api.Extensions
{
    public static class SwaggerRegistrationExtensions
    {
        public static IServiceCollection AddSwagger(this IServiceCollection services)
        {
            return services.AddSwaggerGen(options =>
            {
                options.MapType<FileContentResult>(() => new OpenApiSchema
                {
                    Type = "blob",
                });

                options.SwaggerDoc("v1", new OpenApiInfo() { Title = "Coelsa API", Version = "v1" });
                options.DocInclusionPredicate((docName, description) => true);

                options.AddSecurityDefinition("Bearer",
                    new OpenApiSecurityScheme
                    {
                        Description = "JWT Authorization header using the Bearer scheme. \r\n\r\n Enter 'Bearer' [space] and then your token in the text input below.\r\n\r\nExample: \"Bearer 12345abcdef\"",
                        Name = "Authorization",
                        In = ParameterLocation.Header,
                        Type = SecuritySchemeType.ApiKey,
                        Scheme = "Bearer"
                    }
                );

                options.AddSecurityRequirement(new OpenApiSecurityRequirement {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            },
                            Scheme = "oauth2",
                            Name = "Bearer",
                            In = ParameterLocation.Header
                        },
                        new List<string>()
                        }
                    }
                );
            });
        }

        public static IApplicationBuilder UseSwagger(this IApplicationBuilder app, IConfiguration configuration)
        {
            // Enable middleware to serve generated Swagger as a JSON endpoint
            return app.UseSwagger()
            // Enable middleware to serve swagger-ui assets (HTML, JS, CSS etc.)
            .UseSwaggerUI(options =>
            {
                options.SwaggerEndpoint(configuration["App:ServerRootAddress"] + "swagger/v1/swagger.json", "Coelsa API V1");
            }); // URL: /swagger
        }
    }
}
