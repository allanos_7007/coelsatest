﻿using Coelsa.Dal.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Coelsa.Api.Extensions
{
    public static class DbContextRegistrationExtension
    {
        public static IServiceCollection AddDbContextPool(this IServiceCollection services, IConfiguration configuration)
        {
            var conn = configuration.GetSection("ConnectionStrings:Default");

            return services.AddDbContextPool<CoelsaDbContext>(options => {
                options.UseSqlServer(conn.Value,
                    providerOpcions =>
                    {
                        providerOpcions.EnableRetryOnFailure();
                        providerOpcions.MigrationsAssembly("Coelsa.Dal");
                    });
            });
        }
    }
}
