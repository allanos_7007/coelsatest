﻿using Coelsa.Dal.Core;
using Coelsa.Dal.Repositories;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Coelsa.Api.Extensions
{
    public static class RepositoryRegistrationExtension
    {
        public static IServiceCollection InjectRepositories(this IServiceCollection services)
        {
            // UnitOfWOrk
            services.AddScoped<IUnitOfWork, UnitOfWork>();

            // Repositories
            services.AddScoped<IContactRepository, ContactRepository>();

            return services;
        }
    }
}
