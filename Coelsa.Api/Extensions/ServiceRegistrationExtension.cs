﻿using Coelsa.Bussiness.Services;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Coelsa.Api.Extensions
{
    public static class ServiceRegistrationExtension
    {
        public static IServiceCollection InjectServices(this IServiceCollection services)
        {
            services.AddScoped<IContactService, ContactService>();

            return services;
        }
    }
}
