﻿using Coelsa.Bussiness.Dto;
using Coelsa.Bussiness.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Coelsa.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContactController : ControllerBase
    {
        private readonly IContactService service;

        public ContactController(IContactService service)
        {
            this.service = service;
        }


        [HttpGet("{id}")]
        public async Task<ActionResult<ContactDto>> Get(int id)
        {
            return Ok(await this.service.GetAsync(id));
        }

        [HttpGet]
        public async Task<ActionResult<ICollection<ContactDto>>> Get()
        {
            return Ok(await this.service.GetAsync());
        }

        [HttpPost]
        public async Task<ActionResult<ContactDto>> Create([FromBody] ContactDto request)
        {
            return Ok(await this.service.Add(request));
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<ContactDto>> Update(int id, [FromBody] ContactDto request)
        {
            return Ok(await this.service.Update(id, request));
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<ContactDto>> Remove(int id)
        {
            return Ok(await this.service.Remove(id));
        }
    }
}
