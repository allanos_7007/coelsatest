﻿using AutoMapper;
using Coelsa.Bussiness.Core;
using Coelsa.Bussiness.Dto;
using Coelsa.Dal.Core;
using Coelsa.Dal.Entities;
using Coelsa.Dal.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Coelsa.Bussiness.Services
{
    public class ContactService : BaseService<Contact, ContactDto>, IContactService
    {
        public ContactService(IContactRepository repository, IUnitOfWork uow, IMapper mapper)
        : base(repository, uow, mapper)
        {
        }
    }
}
