﻿using Coelsa.Bussiness.Core;
using Coelsa.Bussiness.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Coelsa.Bussiness.Services
{
    public interface IContactService : IBaseService<ContactDto>
    {
    }
}
