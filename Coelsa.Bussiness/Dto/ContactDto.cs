﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Coelsa.Bussiness.Dto
{
    public class ContactDto
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Company { get; set; }
        public string Email { get; set; }
        public int PhoneNumber { get; set; }
    }
}
