﻿using AutoMapper;
using Coelsa.Bussiness.Dto;
using Coelsa.Dal.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Coelsa.Bussiness.Core
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<ContactDto, Contact>().ForMember(x => x.Id, opt => opt.Ignore());
            CreateMap<Contact, ContactDto>();
        }
    }
}
