﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Coelsa.Bussiness.Core
{
    public interface IBaseService<TDto>
        where TDto : class
    {
        Task<TDto> Add(TDto dto);

        Task<bool> Remove(int id);

        Task<TDto> Update(int id, TDto dto);

        Task<TDto> GetAsync(int id);

        Task<IEnumerable<TDto>> GetAsync();
    }
}
