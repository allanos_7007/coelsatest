﻿using AutoMapper;
using Coelsa.Dal.Core;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Coelsa.Bussiness.Core
{
    public class BaseService<TEntity, TDto> : IBaseService<TDto>
        where TEntity : class
        where TDto : class
    {
        protected IBaseRepository<TEntity> repository;
        protected IMapper mapper;
        protected IUnitOfWork uow;

        public BaseService(IBaseRepository<TEntity> repository,
                           IUnitOfWork unitOfWork,
                           IMapper mapper)
        {
            this.repository = repository;
            this.mapper = mapper;
            this.uow = unitOfWork;
        }

        public virtual async Task<TDto> Add(TDto dto)
        {
            var entity = mapper.Map<TEntity>(dto);
            var result = await this.repository.Add(entity);
            await this.uow.CommitAsync();

            return mapper.Map<TDto>(result);
        }

        public async Task<bool> Remove(int id)
        {
            var entity = await this.repository.GetAsync(id);
            this.repository.Remove(entity);

            return await this.uow.CommitAsync();
        }

        public virtual async Task<TDto> Update(int id, TDto dto)
        {
            var entity = await this.repository.GetAsync(id);
            mapper.Map(dto, entity);
            await this.uow.CommitAsync();

            return mapper.Map<TDto>(entity);

        }

        public virtual async Task<TDto> GetAsync(int id)
        {
            var result = await this.repository.GetAsync(id);

            return mapper.Map<TDto>(result);
        }

        public virtual async Task<IEnumerable<TDto>> GetAsync()
        {
            var result = await this.repository.GetAsync();

            return mapper.Map<IEnumerable<TDto>>(result);
        }
    }
}
